# gtk-kt

`gtk-kt` provides native bindings for the 
[Kotlin](https://kotlinlang.org/) language.

Please check out [gtk-kn](https://gitlab.com/gtk-kn/gtk-kn) as a possible successor project.

---

![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

[Documentation](https://docs.gtk-kt.org)
