# Contact

Come hang out with us on our offical pages

- [Matrix]
- [GitLab]

## Developers

To contact the gtk-kt developers directly, 
 you can either join the [Matrix] space or create an issue on [GitLab].

Please keep in mind that the GitLab is for bug reports or feature requests, 
 not for general support.


----
[Matrix]: https://matrix.to/#/#gtk-kt:matrix.org
[GitLab]: https://gitlab.com/gtk-kt